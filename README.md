# A03Mulakala running personal website on a local server


## How to use

Open a command window in your A03Mulakala folder.

Run npm install to install all the dependencies in the package.json file.

Run node server.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node server.js
```

Point your browser to `http://localhost:8081`.